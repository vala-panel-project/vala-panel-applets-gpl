Vala Panel Applets
===

This is applets for [Vala Panel](https://github.com/rilian-la-te/simple-panel), which has GPL license
(because I borrowed some GPL code and developers refuse to provide this code as LGPL).

Dependencies:
---

*Core:*
 * GLib (>= 2.56.0)
 * GTK3 (>= 3.22.0)
 * valac
 * vala-panel (>= 0.4.60)


Lastly, always set `-DCMAKE_INSTALL_PREXIX=/usr` when using cmake. Otherwise you
won't be able to start the panel on most distros.

Author
---
 * Athor <ria.freelander@gmail.com>

Special thanks
---
 * [Ikey Doherty](mailto:ikey@evolve-os.com) for icontasklist.
