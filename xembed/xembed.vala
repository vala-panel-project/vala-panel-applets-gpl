/*
 * vala-panel
 * Copyright (C) 2015 Konstantin Pugin <ria.freelander@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ValaPanel;
using Gtk;
using XEmbed;

/* Standards reference:  http://standards.freedesktop.org/systemtray-spec/ */
public class XEmbedTray: Applet
{
    private XEmbed.Plugin plugin;
    public override void constructed()
    {
        plugin = new XEmbed.Plugin(this);
        if (plugin == null || plugin.plugin == null || !(plugin.plugin is Widget))
            return;
        this.add(plugin.plugin);
        plugin.plugin.orientation = (toplevel.orientation == Orientation.HORIZONTAL) ? Orientation.VERTICAL:Orientation.HORIZONTAL;
        toplevel.notify["panel-gravity"].connect((o,a)=> {
            plugin.plugin.orientation = (toplevel.orientation == Orientation.HORIZONTAL) ? Orientation.VERTICAL:Orientation.HORIZONTAL;
        });
        this.show();
        plugin.plugin.show();
        plugin.plugin.queue_resize();
    }
    public override void destroy()
    {
        plugin = null;
        base.destroy();
    }
} // End class

[ModuleInit]
public void g_io_xembed_load(GLib.TypeModule module)
{
    // boilerplate - all modules need this
    GLib.IOExtensionPoint.implement(ValaPanel.APPLET_EXTENSION_POINT,typeof(XEmbedTray),"org.valapanel.xembed",10);
}

public void g_io_xembed_unload(GLib.IOModule module)
{
    // boilerplate - all modules need this
}
